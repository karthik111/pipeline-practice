FROM ubuntu:20.04
WORKDIR /app
RUN apt-get update && \
    apt-get install -y curl wget git
CMD [ "bash" ]
